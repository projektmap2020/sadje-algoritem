#include <iostream>
#include <fstream>
#include <vector>
#include <string>

int steviloVsehSadezev = 0;
int steviloJablokNaVoljo = 0;
int mestoPobiranja = 0;
int steviloPobranih = 0;

std::vector<bool> readFile() {
    std::vector<bool> out;
    std::ifstream vhod;
    vhod.open("sadje.02.in");
    std::string t;
    int stevec = 0;
    if (vhod.is_open()) {
        while (vhod >> t) {
            if (stevec == 0) {
                steviloVsehSadezev = std::stoi(t);
            } else if (stevec == 1) {
                steviloJablokNaVoljo = std::stoi(t);
            } else if (stevec == 2) {
                mestoPobiranja = std::stoi(t);
            } else {
                for (char c : t) {
                    if (c == '0') {
                        out.push_back(false);
                    } else if (c == '1') {
                        out.push_back(true);
                    }
                }
            }
            stevec++;
        }
        vhod.close();
    }
    return out;
}

std::vector<int> doBruteforce(std::vector<bool> zaporedje) {
    std::vector<int> out;
    for (int i = 0; i < zaporedje.size(); i++) {
        if (zaporedje.at(i) && i % mestoPobiranja == 0 && i != 0 && steviloJablokNaVoljo > 0) {
            if (!zaporedje.at(i - 1)) {
                std::vector<bool>::iterator mestoVrivanja = zaporedje.begin() + i - 1;
                zaporedje.insert(mestoVrivanja, true);
                out.push_back(i - 1);
                steviloJablokNaVoljo--;
                i++;
            }
        }
    }
    return out;
}

void pickApples(std::vector<bool> zaporedje) {
    for (int i = 0; i < zaporedje.size(); i++) {
        if (i % mestoPobiranja == 0 && i != 0) {
            if (zaporedje.at(i)) {
                steviloPobranih++;
            }
        }
    }
}

int main() {
    std::vector<bool> zaporedje = readFile();
    std::vector<int> mestaVrivanja = doBruteforce(zaporedje);
    pickApples(zaporedje);

    std::cout << steviloPobranih << std::endl;
    for (int stev : mestaVrivanja) {
        std::cout << stev << " ";
    }
    std::cout << std::endl;
    
	return 0;
}